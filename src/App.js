

import React from "react";
import {BrowserRouter as Router, Switch, Route} from "react-router-dom"
import Home from "./pages";
import Article from "./pages/article";
// import ToDo from './components/todo';


function App() {

  // const todo = [
  //   {id: 1, title: "wash dishes", completed: false},
  //   {id: 2, title: "eat breakfast", completed: true}
  // ]
  return (

    <Router>
    <Switch>
    <Route path='/' component={Home} exact />
    <Route path="/article/:id" component={Article} />

        {/* {todo.map((todo) => {
      return (<ToDo todo={todo}/>)
    })} */}


    </Switch>
    </Router>

  );
}

export default App;
