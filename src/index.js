import React from 'react';
import ReactDOM from 'react-dom';
import GlobalStyles from './styles';
// import './index.css';
import App from './App';
import { ApolloProvider, ApolloClient, InMemoryCache } from '@apollo/client';

const client = new ApolloClient({
  uri: 'http://localhost:4000',
  cache: new InMemoryCache(),
});

ReactDOM.render(
  <ApolloProvider client={client}>
  <GlobalStyles />
    <App />
    </ApolloProvider>,
  document.getElementById('root')
);

