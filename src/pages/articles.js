import React from 'react';
import { useQuery, gql } from '@apollo/client';
import QueryResult from '../components/query-result';
import GetArticles from '../components/Articles';

/** ARTICLES gql query to retrieve all most popular articles */
export const ARTICLES = gql`
  query getArticles {
    Articles {
        id
        title
        byline
        published_date
        media {
          mediaMetadata {
            url
            height
            width
          } 
          }
          
        }
      }
`;


const Articles = () => {
  const { loading, error, data } = useQuery(ARTICLES);

  return (
      <QueryResult error={error} loading={loading} data={data}>
        {data?.Articles?.map((article, index) => (
          <GetArticles key={article.id} article={article} />
        ))}
      </QueryResult>
  );
};

export default Articles;