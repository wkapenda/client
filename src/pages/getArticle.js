import React from 'react';
import { useQuery, gql } from '@apollo/client';
import QueryResult from '../components/query-result';
import Article from '../components/Article';

/** ARTICLES gql query to retrieve all most popular articles */
export const ARTICLE = gql`
  # query getArticle 
  
  query getArticle ($articleId: ID!) {
    Article (id: $articleId)  {
    id
    url
    title
    abstract
    byline
    media {
      caption
      copyright
        mediaMetadata {
          url
          height
          width
        } 
      }
    source
    published_date
    updated
    section
    subsection
  }
      }
`;


const GetArticle = (props) => {

  const articleId = props.id;

  const {loading, error, data } = useQuery(ARTICLE
    , { 
    variables: { articleId },
  });

  // console.log(data)


  return (
      <QueryResult error={error} loading={loading} data={data}>
 
          <Article key={articleId} article={data} />

      </QueryResult>
  );
};

export default GetArticle;
