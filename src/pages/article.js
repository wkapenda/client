import React, {useState} from 'react'
import Navbar from '../components/Navbar'
import Sidebar from '../components/Sidebar'
import Footer from "../components/Footer/index"
import GetArticle from "./getArticle"

const Article = (props) => {

    const [isOpen, setIsOpen] = useState(false)

    const toggle = () => {
        setIsOpen(!isOpen);
    }

    return (
        <div>
        <Sidebar isOpen={isOpen} toggle={toggle}/>
        <Navbar toggle={toggle} />

        <GetArticle id={props.match.params.id}/>
        
        <Footer  /> 

        </div>
    )
}

export default Article