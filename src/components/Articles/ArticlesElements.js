import styled from "styled-components"
import { Link } from "react-router-dom"
import { IoIosArrowForward } from "react-icons/io"

export const ArticleContainer = styled.section `

padding:2rem;
display: flex;
flex-direction: row;
flex-wrap: nowrap;
justify-content: space-between;
align-content: left;
${'' /* margin-left: auto;
margin-right: auto; */}
padding-top: 6rem;
padding-bottom: 4rem;
width: 100vw;

@media screen and (max-width: 420px){
  padding-left: 5px ;
  padding-top: 4rem;
padding-bottom: 2rem;

  padding-right: 5px ;
}




`;

export const ArticleWrapper = styled.div `
display: flex;
margin-left: auto;
margin-right: auto;
width: 83%;
${'' /* padding: 0 12px; */}
max-width: 1100px;


`;




export const ImageContainer = styled.div`
max-width: 11rem;
order: 1;
cursor: pointer;
${'' /* text-decoration: none; */}

`;
export const ArticleImage = styled.img`
border-color: #A2DBFA; 
  ${'' /* width: 100%;
  height: auto;

@media screen and (max-width: 769px){
  width: 60%;
  height: auto;
} */}



`;

export const TextContainer = styled.div `
order: 2;
display: grid;
margin-left: 2%;
margin-right: auto;
max-width: 90%;
padding: 0 1.5rem;

`;

export const TextWrapper = styled.div `

align-content: left;
justify-self: start;
text-align: left;
${'' /* margin-left: auto;
margin-right: auto; */}
${'' /* text-align: center; */}
max-width: 1000px;
padding: 0 1.5rem;

@media screen and (max-width: 420px){
  padding: 0 ;
}

`;

export const ButtonContainer = styled(Link) `
display: flex;
order: 3;
${'' /* align-items: center; */}
cursor: pointer;
text-decoration: none;

`;

export const ForwardArrow = styled(IoIosArrowForward) `
width: 40px;
${'' /* height: 40px;
padding: 0;
margin-top:2rem;
margin-bottom: 1rem;
display: block;
margin-left: auto;
margin-right: auto; */}
`;


export const TitleH2 = styled.h2`
font-size: 1.1em;
padding-bottom: 10px;
display: block;
${'' /* margin-left: auto;
margin-right: auto; */}
${'' /* text-align: center; */}

@media screen and (max-width: 769px){
  font-size: 1em;
  text-align: center;
  margin-left: 0;
  padding-left: 0;


  @media screen and (max-width: 420px){
  font-size: 0.4em;
  text-align: center;
  padding: 0px 0px;
}

`

export const AuthorH4 = styled.h4`
font-size: 0.9em;
padding-bottom: 10px;
text-align: left;

@media screen and (max-width: 769px){
  font-size: 0.7em;
  text-align: center;
}

@media screen and (max-width: 420px){
  font-size: 0.4em;
  text-align: center;
}

`

export const DateP = styled.p`
font-size: 0.9em;
text-align: left;

@media screen and (max-width: 769px){
  font-size: 0.7em;
  text-align: center;
}

@media screen and (max-width: 420px){
  font-size: 0.4em;
  text-align: center;
}

`
