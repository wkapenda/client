import React from 'react'
import { BiCalendarEvent } from "react-icons/bi"
import ImgNotAvailable from '../../images/imageNot.png';

import {
    ArticleContainer,
    ArticleWrapper,
    ImageContainer,
    ArticleImage,
    TextContainer,
    TextWrapper,
    ButtonContainer,
    AuthorH4,
    TitleH2,
    DateP,
    ForwardArrow
}
from "./ArticlesElements"

const Articles = ({ key, article }) => {
    
    

    const { id, title, byline, published_date, media } = article;

    const mediaArr = media.map(x => x.mediaMetadata );
    const mediaObj = mediaArr.map(x => x[1]);

    let url = "";
    mediaObj.forEach(function(element) {
      url = element.url;
    })

    return (
        <> 

        <ArticleContainer className="border">
        <ArticleWrapper className="articleWrap">
        <ImageContainer> 
            <ArticleImage src={url ? url : ImgNotAvailable} 
            className="rounded-circle shadow"
            alt="Article Image" 
            style={{width: "100%", height: "auto"}}
            />
        </ImageContainer>
        <TextContainer>
        <TextWrapper>
            <TitleH2>{title}</TitleH2>
                <AuthorH4>{byline}</AuthorH4>
                <DateP> <BiCalendarEvent style={{marginRight: "0.5rem"}} size={"15px"} />{published_date}</DateP>
        </TextWrapper>
        </TextContainer>
        <ButtonContainer to={`/article/${id}`}>
            <ForwardArrow size={"30px"}/>
        </ButtonContainer>

        </ArticleWrapper>
            
        </ArticleContainer>
        
        
        </>

    )
}

export default Articles
