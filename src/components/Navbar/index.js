import React, {useState, useEffect} from 'react';
import {IconContext} from "react-icons/lib"
import {animateScroll as scroll} from "react-scroll"
import LogoIcon from "../../images/nytimeslogo.png";


import { 
    Nav, 
    NavbarContainer, 
    LogoImg, 
    NavLogo, 
    MobileIcon, 
    LeftContent, 
    RightContent, 
    NameP,
    SearchIcon,
     DotsVerticalIcon 
    } from "./NavbarElements";

//rafpce

const Navbar = ({ toggle }) => {

    const [scrollNav, setScrollNav] = useState(false);

    const changeNav = () => {
        if(window.scrollY >= 80) {
            setScrollNav(true)
        }else {
            setScrollNav(false)
        }
    }

    useEffect(() => {
        window.addEventListener("scroll", changeNav)
    }, [])

    const toggleHome = () => {
        scroll.scrollToTop();

    }

    return (
        <>
        <IconContext.Provider value={{ color: "#fff" }}>

        <Nav className="border-bottom shadow-lg" scrollNav={scrollNav}>
        
        <NavbarContainer>
            <LeftContent>

            <MobileIcon onClick={toggle} size="1.5rem"/>

            <NavLogo to='/' onClick={toggleHome}> 
                <LogoImg src={LogoIcon} />
            </NavLogo>

             <NameP>NY Time Most Popular Articles</NameP>

            </LeftContent>

            <RightContent>

            <SearchIcon size="1.5rem"/>
            <DotsVerticalIcon size="1.5rem"/> 

            </RightContent>

        </NavbarContainer>
        


        </Nav>
        {/* </NavbarBorder> */}
        </IconContext.Provider>
            
        </>
    )
}

export default Navbar
