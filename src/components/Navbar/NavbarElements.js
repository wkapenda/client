import styled from "styled-components"
import { Link as LinkR } from "react-router-dom"
// import { Link as LinkS } from "react-scroll"
import {FaBars} from "react-icons/fa"
import { BiSearch, BiDotsVerticalRounded } from "react-icons/bi"



export const Nav = styled.nav `
background: #A2DBFA;
height: 80px;
display: flex;
justify-content: center;
align-items: center;
font-size: 1rem;
position: sticky;
top: 0;
z-index: 10;
border-width: 5px;

    @media screen and (max-width: 960px){
        transition: 0.8s all ease;
    }

`;

export const NavbarContainer = styled.div `
display: flex;
justify-content: space-between;
height: 80px;
width: 100%;
padding: 0 12px;
max-width: 1200px;
z-index: 1;

`;

export const LogoImg = styled.img `
height: 45px;
${'' /* width: 150px; */}
${'' /* margin-top: 15px;
margin-bottom: 15px; */}
`;

export const NavLogo = styled(LinkR)`
color: #fff;
justify-self: flex-start;
cursor: pointer;
font-size: 1.5rem;
display: flex;
align-items: center;
margin-left: 12px;
font-weight: bold;
text-decoration: none;

`;

export const MobileIcon = styled(FaBars)`

fill: black;
display: none;

    &:hover {
        fill: white;
        transition: 0.2s ease-in-out;

    }

    @media screen and (max-width: 768px){
    display: flex;
    cursor: pointer;
    margin-left: 12px;
    
}

`;

export const LeftContent = styled.div`
display: flex;
display: flex;
justify-content: center;
align-items: center;

`;

export const RightContent = styled.div`
display: flex;
justify-content: center;
align-items: center;

`;

export const NameP = styled.p`

margin-top: 1.1rem;
margin-left: 1rem;

`;


export const SearchIcon = styled(BiSearch)`

fill: black;

    &:hover {
        fill: white;
        transition: 0.2s ease-in-out;
    }

`;

export const DotsVerticalIcon = styled(BiDotsVerticalRounded)`

fill: black;
margin-left: 1rem;
margin-right: 12px;

    &:hover {
        fill: white;
        transition: 0.2s ease-in-out;

    }

`;

