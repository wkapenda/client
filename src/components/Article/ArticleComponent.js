import styled from "styled-components"
import { Link } from "react-router-dom"
import { IoIosArrowForward } from "react-icons/io"

export const ArticleContainer = styled.section `

padding:0rem 8rem;
display: grid;
flex-direction: row;
flex-wrap: nowrap;
justify-content: center;
align-content: center;
margin-left: auto;
margin-right: auto;
padding-top: 4rem;
padding-bottom: 4rem;
width: 100vw;

@media screen and (max-width: 420px){
  padding-left: 5px;
  padding-top: 4rem;
  padding-bottom: 2rem;
  padding-right: 5px ;
}

`;

export const TitleContainer = styled.div `
${'' /* display: flex;
margin-left: auto;
margin-right: auto;
width: 83%;
padding: 0 12px;
max-width: 1100px; */}
display: flex;
justify-content: center;
align-content: center;


`;

export const TitleH1 = styled.h1`
text-align: center;
padding-bottom: 1.5rem;


@media screen and (max-width: 769px){
  font-size: 2em;

@media screen and (max-width: 420px){
  font-size: 1em;

}

`;

export const ImageContainer = styled.div`



`;

export const ImageWrapper = styled.div`
display: flex;
justify-content: center;
align-content: center;
padding: 0 10%;


`;

export const ArticleImage = styled.img`
  ${'' /* width: 100%;
  height: auto;

@media screen and (max-width: 769px){
  width: 60%;
  height: auto;
} */}

`;

export const DatesContainer = styled.div `

display: grid;
justify-content: left;
align-content: left;
padding-top: 0.5rem;
${'' /* margin-left: 4rem; */}
${'' /* margin-right: auto; */}
max-width: 100%;
${'' /* padding: 0 1rem; */}

`;

export const DateP = styled.p`

font-size: 0.9em;
text-align: left;
padding-left: 2rem;

@media screen and (max-width: 769px){
  font-size: 0.4em;

}

@media screen and (max-width: 420px){
  font-size: 0.2em;

}

`

export const AbstractH5 = styled.h5`
text-align: left;
padding-bottom: 1.5rem;
font-size: 1.2em;


@media screen and (max-width: 769px){
  font-size: 1em;
  text-align: center;

@media screen and (max-width: 420px){
  font-size: 0.8em;
  text-align: center;

}

`;

export const ArticleLink = styled.a `
padding-bottom: 1.5rem;
text-align: left;


@media screen and (max-width: 420px){


  size: 0.8em;
}

`;


export const CreditsContainer = styled.div `

display: grid;
justify-content: left;
align-content: left;

max-width: 100%;
${'' /* padding: 0 1rem; */}

`;

export const CreditsP = styled.p`

font-size: 0.9em;
text-align: left;


@media screen and (max-width: 769px){
  font-size: 0.7em;

}

@media screen and (max-width: 420px){
  font-size: 0.6em;

}

`

export const SectionSpan = styled.span `
margin-right: 0.5rem;

`


