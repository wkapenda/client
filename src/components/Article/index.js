import React from 'react'
import ImgNotAvailable from '../../images/imageNot.png';

import {
    ArticleContainer,
    TitleContainer,
    TitleH1,
    ImageContainer,
    ImageWrapper,
    ArticleImage,
    DatesContainer,
    DateP,
    AbstractH5,
    ArticleLink,
    CreditsContainer,
    CreditsP,
    SectionSpan
}
from "./ArticleComponent"

const Article = ({key, article}) => {

    const { 
        title, 
        url,
        abstract,
        byline, 
        published_date, 
        source,
        updated,
        section,
        subsection,
        media 
    } = article.Article;


    const mediaArr = media.map(x => x.mediaMetadata );
    const mediaObj = mediaArr.map(x => x[1]);

    let urlImage = "";
    mediaObj.forEach(function(element) {
      urlImage = element.url;
    })

    console.log(urlImage)



    return (
        <ArticleContainer>
        <TitleContainer>
            <TitleH1>{title}</TitleH1>
        </TitleContainer>

        <ImageContainer>
        <ImageWrapper>
            <ArticleImage src={urlImage ? urlImage : ImgNotAvailable} 
                className="shadow-sm"
                alt="Article Image" 
                style={{width: "100%", height: "auto"}}
                />

        </ImageWrapper> 
        </ImageContainer>

        <DatesContainer>
        <DateP>Published On: {published_date}, Updated On: {updated}</DateP>
        </DatesContainer>

        <AbstractH5>{abstract}</AbstractH5>

        <ArticleLink href={url}>Link: {url}</ArticleLink>

        <CreditsContainer>
            <CreditsP>Source: {source}</CreditsP>
            <CreditsP>Author: {byline}</CreditsP>
        </CreditsContainer>

        <CreditsContainer>
            <CreditsP > <SectionSpan className="badge badge-pill badge-primary" 
            style={{fontSize : "0.9em"}}>
            {section}
            </SectionSpan> 
            <SectionSpan className="badge badge-pill badge-info" style={{fontSize : "0.9em"}}>
            {subsection}
            </SectionSpan>
            </CreditsP>
        </CreditsContainer>
    
            
        </ArticleContainer>
    )
}

export default Article
