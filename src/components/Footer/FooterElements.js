import styled from "styled-components"
import { Link } from "react-router-dom"

export const FooterContainer = styled.footer `
padding:2rem;

background: #2980B9;  /* fallback for old browsers */
background: -webkit-linear-gradient(to bottom, #FFFFFF, #6DD5FA, #2980B9);  /* Chrome 10-25, Safari 5.1-6 */
background: linear-gradient(to bottom, #FFFFFF, #6DD5FA, #2980B9); /* W3C, IE 10+/ Edge, Firefox 16+, Chrome 26+, Opera 12+, Safari 7+ */
display: flex;
justify-content: center;
margin-left: auto;
margin-right: auto;
width: 100vw;



`;

export const FooterWrapper = styled.section `
${'' /* max-width: 1250px;
width: 100%; */}
`;



export const LogoWrap = styled(Link) `

${'' /* align-items: center; */}
cursor: pointer;
text-decoration: none;

`;

export const NYLogo = styled.img `
height: 40px;
padding: 0;
margin-top:2rem;
margin-bottom: 1rem;
display: block;
margin-left: auto;
margin-right: auto;
`;

export const WebsiteRights = styled.small `
font-size: 0.8em;
display: block;
margin-left: auto;
margin-right: auto;
text-align: center;

@media screen and (max-width: 500px){
  font-size: 0.7em;
  max-width: 300px;
}

`


