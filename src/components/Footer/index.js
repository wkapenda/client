import React from 'react'



import {animateScroll as scroll} from "react-scroll"
import LogoIcon from "../../images/nytimeslogo.png";


import {
    FooterContainer,
    FooterWrapper,
    WebsiteRights,
    LogoWrap,
    NYLogo
} from "./FooterElements";



const Footer = () => {

    let currentYear = new Date().getFullYear();

    const toggleHome = () => {
        scroll.scrollToTop();

    }

    return (
        <FooterContainer>
            <FooterWrapper>
                <LogoWrap to="/" onClick={toggleHome} className="logo">
                <NYLogo src={LogoIcon} />
                </LogoWrap>
                    <WebsiteRights className="copyrights">NY Times Most Popular Articles © {currentYear}. All Rights Reserved. </WebsiteRights>
            </FooterWrapper>

        </FooterContainer>

    )
}

export default Footer
